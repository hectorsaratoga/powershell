



$servidor = "ts02"
$sesiones_inactivas = (quser /server:$servidor | Where-Object { $_ -match "Desc" })

foreach ($item in $sesiones_inactivas) {
    $sessionId = ((quser /server:$servidor | Where-Object { $_ -match "Desc" }) -split ' +')[2]
    logoff $sessionId /server:$servidor

    }